# Docker Jenkins server with build tools
## Included tools
- Sass

#### Docker hub image 
Use `docker pull thilinaperera/ci_server` to get new or modified image.

#### Create your own image
```Dockerfile
docker build -t <image-name> .
docker tag <IMAGE ID> yourhubusername/reponame
docker push yourhubusername/reponame
```
#### Use as a service (Docker swarm mode)
```
docker service create --name jenkins -p 8080:8080 -p 50000:50000 thilinaperera/ci_server
```
with volume bind
```
docker service create --name jenkins -p 8080:8080 -p 50000:50000 --mount "type=bind,source=/local_folder_path,target=/var/jenkins_home" thilinaperera/ci_server
```